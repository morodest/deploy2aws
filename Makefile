.PHONY: terraform
ifndef VERBOSE
.SILENT:
endif

export PATH := ./bin:$(PATH)

# for demo only
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=e6368418-e974-181b-3f55-96ecf328b190

REPO_URI := "954001739846.dkr.ecr.eu-central-1.amazonaws.com/jetbrains"

install_deps: download_deps run_vault_server install_ansible docker_login

download_deps:
	wget https://releases.hashicorp.com/vault/0.8.1/vault_0.8.1_linux_amd64.zip -O vault.zip && \
	unzip -o -d ./bin/ vault.zip
	wget https://releases.hashicorp.com/terraform/0.10.2/terraform_0.10.2_linux_amd64.zip -O terraform.zip && \
	unzip -o -d ./bin/ terraform.zip

install_ansible:
	virtualenv --version >/dev/null || (echo -e "\npython-virtualenv not found\n"; exit)
	virtualenv ./.venv &&  \
	. ./.venv/bin/activate && \
	pip install -U pip && \
	pip install -r requirements.txt && \
	deactivate
	ln -sf ../.venv/bin/ansible-playbook ./bin/ansible-playbook

docker_login:
	$(shell aws ecr get-login)

run_vault_server:
	cd vault_server && \
	vault server -config=vault.hcl >./vault.log 2>&1 &
	sleep 2
	VAULT_SKIP_VERIFY="true" vault unseal UIpZB75iWOFMpJmpdGuxX7u9JR9329z6kGV0XfVjx22c
	VAULT_SKIP_VERIFY="true" vault unseal T06GB2DJSR/MXgi/Yb47QQrXj4PISDRbr2QgPA4lg5WN
	VAULT_SKIP_VERIFY="true" vault unseal ssf2HPFzdbNb7Rmi7wG8cqeLfIJqHxaVEAMj/6EOzFYD

setup_aws_env: terraform_apply docker_setup

terraform_apply: terraform_plan
	cd ./terraform && terraform apply

terraform_plan:
	cd ./terraform && terraform plan

terraform_destroy:
	cd ./terraform && terraform destroy

docker_setup:
	cd ./ansible && ansible-playbook -i ./inventories/aws.py -v \
	-b --become-method=sudo --become-user=root -c paramiko \
	-u ec2-user setup.yml

docker_build: generate_version_txt
	rm -rf docker/blog
	cp -a blog docker/blog && \
	cd ./docker && docker build . -t ${REPO_URI}:$(shell cat blog/version.txt)
	docker push ${REPO_URI}:$(shell cat blog/version.txt)

generate_version_txt:
	echo "$(shell git tag | sort |tail -n1)-$(shell date +%F)" > blog/version.txt

deploy:
	cd ./ansible && ansible-playbook -i ./inventories/aws.py -v \
	-b --become-method=sudo --become-user=root -c paramiko \
	-u ec2-user deploy.yml -v

switch2green:
	cd ./ansible && ansible-playbook -i ./inventories/aws.py -v \
	-b --become-method=sudo --become-user=root -c paramiko \
	-u ec2-user -e expected_env=green switch.yml -v

switch2blue:
	cd ./ansible && ansible-playbook -i ./inventories/aws.py -v \
	-b --become-method=sudo --become-user=root -c paramiko \
	-u ec2-user -e expected_env=blue switch.yml -v
