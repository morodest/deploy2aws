backend "file" {
  path = "vault.backend"
}

listener "tcp" {
  tls_disable = 1
  address = "127.0.0.1:8200"
}

disable_mlock = true
default_lease_ttl = "87600h"
max_lease_ttl = "87600h"
