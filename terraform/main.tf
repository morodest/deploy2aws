provider "aws" {
    region = "${var.region}"
}

resource "aws_key_pair" "jb-user" {
    key_name = "jb-user-key"
    public_key = "${file(var.ssh_pubkey_file)}"
}

resource "aws_vpc" "main" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
}

resource "aws_route_table" "external" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.main.id}"
    }
}

resource "aws_route_table_association" "external-primary" {
    subnet_id = "${aws_subnet.primary.id}"
    route_table_id = "${aws_route_table.external.id}"
}

resource "aws_route_table_association" "external-secondary" {
    subnet_id = "${aws_subnet.secondary.id}"
    route_table_id = "${aws_route_table.external.id}"
}

resource "aws_subnet" "primary" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "10.0.1.0/24"
    availability_zone = "${var.availability_zone["a"]}"
}

resource "aws_subnet" "secondary" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "10.0.2.0/24"
    availability_zone = "${var.availability_zone["b"]}"
}

resource "aws_internet_gateway" "main" {
    vpc_id = "${aws_vpc.main.id}"
}

resource "aws_security_group" "load_balancers" {
    name = "load_balancers"
    description = "Allows all traffic"
    vpc_id = "${aws_vpc.main.id}"

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "ec2-wp" {
    name = "ec2-wp"
    description = "Allows all traffic"
    vpc_id = "${aws_vpc.main.id}"

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        security_groups = ["${aws_security_group.load_balancers.id}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "wordpres" {
    count = "${var.wordpres_count}"
    vpc_security_group_ids = ["${aws_security_group.ec2-wp.id}"]
    subnet_id = "${aws_subnet.primary.id}"
    ami = "${lookup(var.amis, var.region)}"
    instance_type = "${var.instance_type}"
    iam_instance_profile = "${aws_iam_instance_profile.ec2-wp.name}"
    key_name = "${aws_key_pair.jb-user.key_name}"
    associate_public_ip_address = true
    tags = {
      inventory_groups = "${var.inventory_group_wordpres}"
      Name = "${var.inventory_group_wordpres}-${count.index}"
    }
}

resource "aws_elb" "wp-lb" {
    name = "example-elb"

    # The same availability zone as our instance
    subnets = ["${aws_subnet.primary.id}", "${aws_subnet.secondary.id}"]

    security_groups = ["${aws_security_group.load_balancers.id}"]

    listener {
        instance_port     = 80
        instance_protocol = "http"
        lb_port           = 443
        lb_protocol       = "https"
//        ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/ssl-wordpress"
    }

    listener {
        instance_port     = 80
        instance_protocol = "http"
        lb_port           = 80
        lb_protocol       = "http"
    }

    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = 3
        target              = "HTTP:80/"
        interval            = 30
    }

    # The instance is registered automatically

    instances                   = ["${aws_instance.wordpres.*.id}"]
    cross_zone_load_balancing   = true
    idle_timeout                = 400
    connection_draining         = true
    connection_draining_timeout = 400
}

# for example. TODO: add ssl certificate to vault
//resource "aws_iam_server_certificate" "test_cert" {
//    name             = "ssl-wordpress"
//    certificate_body = <<EOF
//        -----BEGIN CERTIFICATE-----
//        [......] # cert contents
//        -----END CERTIFICATE-----
//        EOF
//
//    private_key = <<EOF
//        -----BEGIN RSA PRIVATE KEY-----
//        [......] # cert contents
//        -----END RSA PRIVATE KEY-----
//        EOF
//}

resource "aws_lb_cookie_stickiness_policy" "default" {
    name                     = "lbpolicy"
    load_balancer            = "${aws_elb.wp-lb.id}"
    lb_port                  = 443
    cookie_expiration_period = 600
}

resource "aws_iam_role" "ec2-wp_host_role" {
    name = "ec2-wp_host_role"
    assume_role_policy = "${file("policies/ec2-wp-role.json")}"
}

resource "aws_iam_role_policy" "ec2-wp_instance_role_policy" {
    name = "ec2-wp_instance_role_policy"
    policy = "${file("policies/ec2-wp-instance-role-policy.json")}"
    role = "${aws_iam_role.ec2-wp_host_role.id}"
}

resource "aws_iam_role" "ec2-wp_service_role" {
    name = "ec2-wp_service_role"
    assume_role_policy = "${file("policies/ec2-wp-role.json")}"
}

resource "aws_iam_role_policy" "ec2-wp_service_role_policy" {
    name = "ec2-wp_service_role_policy"
    policy = "${file("policies/ec2-wp-service-role-policy.json")}"
    role = "${aws_iam_role.ec2-wp_service_role.id}"
}

resource "aws_iam_instance_profile" "ec2-wp" {
    name = "ec2-wp-instance-profile"
    path = "/"
    role = "${aws_iam_role.ec2-wp_host_role.name}"
}
