variable "region" {
  description = "The AWS region to create resources in."
  default = "eu-central-1"
}

variable "availability_zone" {
  description = "The availability zone"
  type = "map"
  default = {
    a = "eu-central-1a",
    b = "eu-central-1b"
  }
}

variable "wordpres_count" {
  description = "Count of wordpres instance"
  default = "1"
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the AWS ECS optimized images."
  type = "map"
  default = {
      eu-central-1 = "ami-a3a006cc"
  }
}

variable "instance_type" {
    default = "t2.micro"
}

variable "ssh_pubkey_file" {
  description = "Path to an SSH public key"
  default = "~/.ssh/id_rsa.pub"
}

variable "inventory_group_wordpres" {
  description = "Name of inventory group for wordpres"
  type = "string"
  default = "wordpres"
}


# RDS Instance Variables

variable "rds_instance_identifier" {
    description = "Custom name of the instance"
    default = "wp-database"
}

variable "rds_is_multi_az" {
  description = "Set to true on production"
  default     = false
}

variable "rds_storage_type" {
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD)."
  default     = "standard"
}

variable "rds_iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1', default is 0 if rds storage type is not io1"
  default     = "0"
}

variable "rds_allocated_storage" {
  default = "5"
}

variable "rds_engine_type" {
  default = "mysql"
}

variable "rds_engine_version" {
  default = "5.6.35"
}

variable "rds_instance_class" {
  default = "db.t2.micro"
}

variable "auto_minor_version_upgrade" {
  default     = true
}

variable "allow_major_version_upgrade" {
  default     = false
}

variable "apply_immediately" {
  default     = false
}

variable "maintenance_window" {
  description = "Syntax: 'ddd:hh24:mi-ddd:hh24:mi' UTC "
  default     = "Mon:03:00-Mon:04:00"
}

variable "database_name" {
  type = "string"
  default = "wp_database"
}

//variable "database_user" {}
//
//variable "database_password" {}

variable "database_port" {
  default = "3306"
}

variable "db_parameter_group" {
  description = "Parameter group, depends on DB engine used"
  default = "mysql5.6"
}

variable "publicly_accessible" {
  description = "Determines if database can be publicly available (NOT recommended)"
  default     = false
}

variable "skip_final_snapshot" {
  description = "If true (default), no snapshot will be made before deleting DB"
  default     = true
}

variable "copy_tags_to_snapshot" {
  description = "Copy tags from DB to a snapshot"
  default     = true
}

variable "backup_window" {
  description = "When AWS can run snapshot, can't overlap with maintenance window"
  default     = "22:00-03:00"
}

variable "backup_retention_period" {
  type        = "string"
  description = "How long will we retain backups"
  default     = 0
}

variable "inventory_group_db" {
  default = "database"
}
