#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
module: bg_deploy
short_description: Deploy docker images.
description:
   - Deploy docker images.
version_added: "0.1"
options:
  image:
    description:
      - image name.
    required: true
  current_tag:
    description:
      - current tag.
  next_tag:
    description:
      - next version tag.
    required: true
  enabled_tag:
    description:
      - enabled tag.
  enabled_env:
    description:
      - enabled env (blue/green).
author: "Vasiliy Mosunov"
'''

EXAMPLES = '''
- bg_deploy: 
    image: "xxxxxxx.dkr.ecr.eu-central-1.amazonaws.com/name" 
    current_tag: "1.0.1-2017-08-21" 
    next_tag: "1.1.1-2017-08-21"
    enabled_tag: "1.0.1-2017-08-21" 
    enabled_env: "blue" 

'''
import logging.config
import pprint

from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

pp = pprint.PrettyPrinter(indent=4).pprint

logging.basicConfig(filename='/tmp/bg_deploy.log',
                    format='%(asctime)s %(message)s',
                    level=logging.DEBUG)


class Config(object):
    LOCAL_ENV_FILE = '/etc/current_env'

    def __init__(self, params):

        self.image = params['image']
        self.current_tag = params['current_tag']
        self.next_tag = params['next_tag']
        self.enabled_tag = params['enabled_tag']

        if not self.get_local_env(params['enabled_tag']):
            self.local_www_env = 'blue'
            self.local_deploy_env = 'green'

        if params['enabled_env'] == 'green':
            self.enabled_tag_env = 'green'
        else:
            self.enabled_tag_env = 'blue'

        if self.current_tag == self.next_tag:
            self.updatable = False
        else:
            self.updatable = True

    def get_local_env(self, state_tag):
        if not os.path.isfile(self.LOCAL_ENV_FILE):
            return None

        with open(self.LOCAL_ENV_FILE, 'r') as state:
            local_env = state.read()

        if local_env == 'green':
            self.local_www_env = 'green'
            self.local_deploy_env = 'blue'
        else:
            self.local_www_env = 'blue'
            self.local_deploy_env = 'green'


class Job(object):

    NGINX_INC_PATH = '/etc/nginx/conf.d/upstream.inc'
    BLUE_INC_PATH = '/etc/nginx/conf.d/upstream.inc_blue'
    GREEN_INC_PATH = '/etc/nginx/conf.d/upstream.inc_green'
    NGINX_ENV = {
        'blue': BLUE_INC_PATH,
        'green': GREEN_INC_PATH
    }

    def __init__(self, cfg, module):
        self.cfg = cfg
        self.m = module
        self.m._debug = True
        self.msg = []

    def run(self):
        if not self.check_conditions_for_update():
            return False, 'Nothing to update'

        ch_www = self.update_docker(self.cfg.local_www_env,
                           self.cfg.current_tag,
                           force=False)
        self.m.log('ch_www')
        ch_deploy = self.update_docker(self.cfg.local_deploy_env,
                           self.cfg.next_tag,
                           force=True)
        self.m.log('ch_deploy')

        nginx_current_env = self.check_nginx_include()
        self.m.log('current nginx env: {}'.format(nginx_current_env))
        ch_nginx = False
        if not nginx_current_env or nginx_current_env != self.cfg.enabled_tag_env:
            ch_nginx = self.switch_nginx_to_default_env(self.cfg.local_www_env)
        self.m.log('change nginx: {}'.format(ch_nginx))
        self.set_local_env()
        if ch_www or ch_deploy or ch_nginx:
            return True, ''.join(self.msg)
        return False, 'No changes'

    def check_nginx_include(self):
        if os.path.islink(self.NGINX_INC_PATH):
            real_inc = os.readlink(self.NGINX_INC_PATH)
            if real_inc == self.BLUE_INC_PATH:
                return 'blue'
            elif real_inc == self.GREEN_INC_PATH:
                return 'green'
        return None

    def check_conditions_for_update(self):
        if not self.cfg.updatable:
            return False
        if not self.cfg.next_tag:
            return False
        return True

    def switch_nginx_to_default_env(self, env):
        try:
            if os.readlink(self.NGINX_INC_PATH) == self.NGINX_ENV[env]:
                return False
        except OSError:
            pass
        if os.path.isfile(self.NGINX_INC_PATH):
            os.remove(self.NGINX_INC_PATH)
        os.link(self.NGINX_ENV[env], self.NGINX_INC_PATH)
        self.restart_nginx()
        self.msg.append('\nUpdate nginx env ({})\t'.format(env))
        return True

    def restart_nginx(self):
        cmd_test = '{} -t'.format(self.m.get_bin_path('nginx'))
        self.m.run_command(cmd_test, check_rc=True)
        cmd = '/etc/init.d/nginx restart'
        self.m.run_command(cmd, check_rc=True)

    def update_docker(self, env, tag, force=False):
        docker_ps = self.get_runnig_docker(env, tag)
        if docker_ps and tag in docker_ps:
            return False
        elif docker_ps and force:
            self.docker_stop(env)
        self.docker_run(env, tag)
        self.msg.append('\nrun docker: env={}, tag={}\t'.format(env, tag))
        return True

    def get_runnig_docker(self, env, tag):
        cmd = r"{dockerbin} ps -a --format '{{{{.Names}}}}\t{{{{.Image}}}}\t{{{{.Status}}}}'".format(
            dockerbin=self.m.get_bin_path('docker'),
        )
        rc, out, err = self.m.run_command(cmd, check_rc=False)
        if not rc and env in out:
            return out
        return None

    def docker_stop(self, env):
        cmd = '{dockerbin} rm -f {env}'.format(
            dockerbin=self.m.get_bin_path('docker'),
            env=env
        )
        self.m.run_command(cmd, check_rc=False)

    def docker_run(self, env, tag):
        if env == 'green':
            port = '9002'
        else:
            port = '9001'
        cmd = '{dockerbin} run --restart unless-stopped --env-file /etc/rds.conf --name {env} ' \
              'deploy.yml-p 127.0.0.1:{port}:80 -d {image}:{tag}'.format(
            dockerbin=self.m.get_bin_path('docker'),
            env=env,
            port=port,
            image=self.cfg.image,
            tag=tag
        )
        self.m.run_command(cmd, check_rc=True)

    def set_local_env(self):
        with open(self.cfg.LOCAL_ENV_FILE, 'w') as state:
            state.write(self.cfg.local_www_env)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            image=dict(required=True),
            current_tag=dict(required=True),
            next_tag=dict(required=True),
            enabled_tag=dict(required=True),
            enabled_env=dict(required=True),
        ),
        supports_check_mode=True
    )

    conf = Config(module.params)
    job = Job(conf, module)
    try:
        changed, comments = job.run()
    except Exception, e:
        module.fail_json(msg=str(e))

    module.exit_json(changed=changed, msg=("All done. {}".format(comments)))

if __name__ == '__main__':
    main()
