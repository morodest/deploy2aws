#!/usr/bin/env python
"""
anwible.wrike.inventory.ec2-sp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dynamic inventory file for EC2 storage-proxy
"""
import os
import json
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

from boto3.session import Session

DEFAULTS = {
    'profile_name': 'admin',
    'regions': ['eu-central-1', ],
    'instance_keys': ['Name', 'ROLE', 'GW', 'enabled_tag', 'enabled_env'],
}


def get_cfg():
    ENV_PREFIX = 'AWS_JB'

    def var_name(name):
        return '_'.join([ENV_PREFIX, name])

    return {
        'profile': os.environ.get(var_name('PROFILE'), DEFAULTS['profile_name']),
        'regions': os.environ.get(var_name('REGIONS'), DEFAULTS['regions']),
        'instance_keys': DEFAULTS['instance_keys'],
    }


def main():
    instances = {}
    cfg = get_cfg()
    session = Session(profile_name=cfg['profile'])
    for region in cfg['regions']:
        resource = session.resource('ec2', region_name=region)
        for i in resource.instances.all():
            if not i.tags:
                continue

            if i.state['Name'] != 'running':
                continue

            i_dct = {el['Key']: el['Value'] for el in i.tags}
            if 'inventory_groups' not in i_dct:
                continue
            instance_vars = {k.lower(): i_dct[k] for k in i_dct.keys() if k in cfg['instance_keys']}
            instance_vars.update({
                'region': region,
                'zone': i.placement['AvailabilityZone'],
                'ansible_ssh_common_args': '-l ec2-user -o ProxyCommand="ssh -W %h:%p -q ec2-user@{}"'.format(i_dct['GW'] if 'GW' in i_dct.keys() else ''),

            })
            for inventory_group in i_dct['inventory_groups'].split(','):
                instances.setdefault(inventory_group, {'children': []})
                instances[inventory_group]['children'].append(i.id)
                instances[i.id] = {
            	    'hosts': [i.private_ip_address],
            	    'vars': instance_vars,
            	}
    print json.dumps(instances, sort_keys=True, indent=4, separators=(',', ':')),


if __name__ == "__main__":
    main()
